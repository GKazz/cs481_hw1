import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome To Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
      ),
       body: Center(
         child: Text('''
         Hello World
         Gintas Kazlauskas
         EST Graduation Fall 2021
         Mistakes Make Success
         ''',
           textAlign: TextAlign.center,
         ),
       ),
      ),
    );
  }
}

